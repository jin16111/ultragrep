UltraGrep 

author jin

UltraGrep is a C++ 17 application that recursively searches a files system for text files or code files and list all of the lines that contain a given regular expression.

Interface:
ultragrep [-v] folder expr [extention-list]*

Examples:
ultragrep . word	
searches all of the .txt files of the current directory for the string ‘word’

ultragrep –v c:\user [Gg]et .cpp.hpp.h
searches c:\user folder for the C++ source files containing either the string “Get” or “get”.



Functional Requirements
1. Scans the correct number of files.
2. Reports the correct number of matches.
3. Reports the file containing the match.
4. Reports the line containing the match.
5. Verbose mode prints each file being searched.
6. Verbose mode prints each grep match found.

Non-functional requirements
1. Implements threadpool.
2. Implements both C++ 11 and Win32 threading via an adapter pattern.
 