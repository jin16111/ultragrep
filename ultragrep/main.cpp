/*
author	Jinzhi Yang
Date	2019-11-02
Brief	this application recursively searches a files system for text files or code files and list all of the lines that contain a given regular expression.
*/

#include <fstream>
#include <iostream>
#include <filesystem>
#include <vector>
#include <string>
#include <Windows.h>
#include <algorithm>
#include <cassert>
#include <iomanip>
#include <locale>
#include <numeric>
#include <sstream>
#include <thread>
#include <vector>
#include <regex>
#include "grepper.hpp"

namespace fs = std::filesystem;

using namespace std;
using namespace fs;


int main(int argc, char** argv)
{
	// Configure System
	(void)cout.imbue(locale(""));
	cout << fixed << setprecision(7);

	chrono::high_resolution_clock::time_point start = chrono::high_resolution_clock::now();

	if (argc < 3)
	{
		cout << "Error: not enough args" << endl;
		cout << "\tThe application's interface is: ultragrep[-v] folder expr[extention - list] *" << endl;

		cout << "\tExamples: " << endl;
		cout << "\tultragrep . word" << endl;
		cout << "\t\tsearches all of the.txt files of the current directory for the string 'word'" << endl;
		cout << "\tultragrep -v c : \\user[Gg]et.cpp.hpp.h" << endl;
		cout << "\t\tsearches the user folder for the C++ source files containing either the string 'Get' or 'get'." << endl;

		return EXIT_FAILURE;
	}

	string arg2 = "-v";
	string arg2b = "";

	bool ifVerbose = false;

	string arg_path = "";
	string arg_pattern = "";
	string arg_extension = ".txt";

	if (argc == 3)
	{
		arg_path = argv[1];
		arg_pattern = argv[2];
	}
	else if (argc == 4)
	{
		string arg1 = argv[1];
		if (arg1 == "-v")
		{
			ifVerbose = true;
			arg_path = argv[2];
			arg_pattern = argv[3];
		}
		else
		{
			arg_path = argv[1];
			arg_pattern = argv[2];
			arg_extension = argv[3];
		}
	}
	else if (argc >= 5)
	{
		string arg1 = argv[1];
		if (arg1 == "-v")
		{
			ifVerbose = true;
			arg_path = argv[2];
			arg_pattern = argv[3];
			arg_extension = argv[4];
		}
		else
		{
			arg_path = argv[1];
			arg_pattern = argv[2];
			arg_extension = argv[3];
		}
	}

	if (!fs::exists(arg_path))
	{
		cout << "Error:\n" << arg_path << " doesn't exits" << endl;
		return EXIT_FAILURE;
	}

	//regex pattern
	regex pattern(arg_pattern);

	//regex extensions
	arg_extension.erase(0, 1);											//remove the leading .
	std::replace(arg_extension.begin(), arg_extension.end(), '.', '|'); //replace . with |
	regex extensions(arg_extension, regex::icase);


#ifdef _DEBUG
	cout << "ifVerbose = " << ifVerbose << endl;
	cout << "arg_path = " << arg_path << endl;
	cout << "arg_pattern = " << arg_pattern << endl;
	cout << "arg_extension = " << arg_extension << endl;
#endif

	Grepper g(arg_path, move(pattern), move(extensions), ifVerbose);

	g.grep();

	//reports
	g.print_report();

	chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();

	//summary
	cout << "\nFiles with matches = " << g._report.size() << endl;
	cout << "Total number of matches = " << g._total_count << endl;
	cout << "Scan Completed in " << chrono::duration_cast<chrono::milliseconds>(end - start).count() / 1000.0 << "s\n";

	return EXIT_SUCCESS;
}