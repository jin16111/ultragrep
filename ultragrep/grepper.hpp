#pragma once
/*
Author	Jinzhi Yang
Date	2019.11.01
Brief	Grepper takes a path, recursive scan the path,find the lines match the pattern, and generate a report
*/

#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <string>
#include <fstream>
#include <filesystem>
#include <Windows.h>
#include <algorithm>
#include <cassert>
#include <numeric>
#include <mutex>
#include <regex>
#include <algorithm>
#include <queue>

namespace fs = std::filesystem;
using namespace std;


class ReportItemLine
{
public:
	string line;
	size_t line_num;
	size_t match_num;

	ReportItemLine(size_t ln, string l, size_t match) : line_num(ln), line(l), match_num(match) {}
	string toString() const;
	bool operator<(ReportItemLine const& rhs) const;
};

class ReportItem
{
public:
	string file_name;
	vector<ReportItemLine> lines;
	bool operator<(ReportItem const& rhs) const;
	string toString() const;
};

class FileTaskPackage
{
public:
	using it = vector<fs::directory_entry>::const_iterator;
	it begin, end;
	vector<ReportItem> reports;
	FileTaskPackage(it b, it e) : begin(b), end(e) {}
};

class LineTaskPacage
{
public:
	size_t begin, end;
	vector<ReportItemLine> reportItemLines;
	vector<string>* linesPtr;
	LineTaskPacage(size_t b, size_t e, vector<string>* ptr) : begin(b), end(e), linesPtr(ptr) {}
};


class Grepper
{
public:
	vector<ReportItem> _report;
	atomic<size_t> _file_count;
	atomic<size_t> _total_count;
private:
	string _root_path;
	regex _pattern;
	regex reg_extensions;
	string _extension;
	bool _verbose;

	//scan files in package 
	void scan_file_package_tp(FileTaskPackage& filePackage);

	//scan a lines in package
	void scan_line_package(string file_name, LineTaskPacage& package);

	//get frequency of a pattern in str
	unsigned static get_frequency(string const& str, regex const& wordRgx);

	//locks verbose using mutex
	void static locked_cout(string msg);

public:
	//constructor
	Grepper(string p, regex r, regex e, bool v);
	//grep 
	void grep();

	//print the report
	void print_report();

	//sort report first by file, then by line number
	void sort_report();
};