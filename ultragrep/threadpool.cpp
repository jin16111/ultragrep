/*
author	Jinzhi Yang
Date	2019-11-02
Brief	implementation of ThreadPool
*/

#include "threadpool.hpp"
#ifdef _WIN32_but_I_prefer_cpp11
bool ThreadPool::workTime = true;
HANDLE ThreadPool::wakeEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
queue<function<void()>> ThreadPool::tasks;
CriticalSection ThreadPool::taskMtx;

ThreadPool::ThreadPool(size_t nThreads) {
	vector<HANDLE> threads;
	for (unsigned i = 0; i < nThreads; ++i)
		threads.push_back(CreateThread(NULL, 0, perform_task, NULL, 0, NULL));
}

ThreadPool::~ThreadPool() {
	workTime = false;

	for (size_t i = 0; i < threads.size(); i++)
	{
		SetEvent(wakeEvent);
	}

	// cleanup
	WaitForMultipleObjects((DWORD)threads.size(), threads.data(), TRUE, INFINITE);
	for (auto& t : threads)
		CloseHandle(t);

	CloseHandle(wakeEvent);
}
// perform tasks in a thread
DWORD WINAPI  ThreadPool::perform_task(LPVOID) {
	while (workTime) {
		WaitForSingleObject(wakeEvent, INFINITE);
		while (!tasks.empty()) {
			function<void()> task;
			bool haveTask = false;
			{CSLock lk(taskMtx);
			if (!tasks.empty()) {
				task = tasks.front();
				tasks.pop();
				haveTask = true;
			}
			}
			if (haveTask) {
				task();
			}
		}
	}
	return 0;
}

#else
ThreadPool::ThreadPool(size_t nThreads) : _possibleMoreWork(true)
{
	for (size_t i = 0; i < nThreads; ++i)
		_threads.push_back(thread(&ThreadPool::perform_task, this));
}

ThreadPool::~ThreadPool()
{
	{
		std::unique_lock<std::mutex> lock(_taskMtx);
		_possibleMoreWork = false;
	}
	_wakeCV.notify_all();
	for (std::thread& t : _threads)
		t.join();
}

void ThreadPool::perform_task() {
	for (;;)
	{
		function<void()> task;
		{unique_lock<mutex> lock(_taskMtx);
		_wakeCV.wait(lock, [this]() { return !_possibleMoreWork || !_tasks.empty(); });
		if (!_possibleMoreWork && _tasks.empty())
			return;
		task = move(_tasks.front());
		_tasks.pop();
		}
		task();
	}
}

#endif