/*
author	Jinzhi Yang
Date	2019-11-02
Brief	implementation of Grepper
*/

#include "grepper.hpp"
#include "threadpool.hpp"


mutex console_mtx;
ThreadPool tp(100);


Grepper::Grepper(string p, regex r, regex e, bool v) : _root_path(p), _pattern(r), reg_extensions(e), _file_count(0), _total_count(0), _verbose(v) {}
void Grepper::locked_cout(string msg)
{
	lock_guard<mutex> lk(console_mtx);
	cout << msg;
}

void Grepper::scan_line_package(string file_name, LineTaskPacage& package)
{
	for (auto i = package.begin; i < package.end; i++)
	{
		unsigned freq = get_frequency((*package.linesPtr)[i], _pattern);
		if (freq != 0)
		{
			package.reportItemLines.push_back(ReportItemLine(i + 1, (*package.linesPtr)[i], freq));
			if (_verbose)
				locked_cout("Matched " + to_string(freq) + ": \"" + file_name + "\" [" + to_string(i) + "] " + (*package.linesPtr)[i] + "\n");
		}
	}
}

unsigned Grepper::get_frequency(string const& str, regex const& wordRgx)
{
	unsigned total = 0;
	auto words_itr = sregex_iterator(str.cbegin(), str.cend(), wordRgx);
	auto words_end = sregex_iterator();
	while (words_itr != words_end)
	{
		auto match = *words_itr;
		auto word = match.str();
		if (word.size() > 0)
			++total;
		words_itr = next(words_itr);
	}
	return total;
}

void Grepper::print_report()
{
	cout << "Grep Report:\n\n";
	for (auto const& report : _report)
	{
		cout << report.toString() << endl;
	}
}

void Grepper::sort_report()
{
	// sort lines
	for (auto& item : _report)
	{
		sort(item.lines.begin(), item.lines.end());
	}
	//sort items
	sort(_report.begin(), _report.end());
}

string ReportItem::toString() const
{
	string result = "";
	result += "\"" + file_name + "\"" + "\n";
	for (auto& item : lines)
	{
		result += item.toString() + "\n";
	}
	return result;
}

bool ReportItem::operator<(ReportItem const& rhs) const
{
	return this->file_name < rhs.file_name;
}

string ReportItemLine::toString() const
{
	string matchNumString = "";
	if (match_num > 1)
		matchNumString = ":" + to_string(match_num);
	return "[" + to_string(line_num) + matchNumString + "] " + line;
}

bool ReportItemLine::operator<(ReportItemLine const& rhs) const
{
	return this->line_num < rhs.line_num;
}

void Grepper::grep()
{
	vector<fs::directory_entry> directories;

	for (auto& p : fs::recursive_directory_iterator(_root_path))
	{
		directories.push_back(p);
	}

	size_t nElements = directories.size();

	//get num of threads
	SYSTEM_INFO si;
	GetSystemInfo(&si);
	auto nThreads = si.dwNumberOfProcessors; //dw number is DWORD, DWORD is unsigned long

	//Create tasks
	vector<FileTaskPackage> packages;

	// make groups based on the number of directory entries
	for (size_t i = 0; i < nThreads; i++)
	{
		size_t startOff = size_t(i * nElements / nThreads);
		size_t endOff = size_t((i + 1) * nElements / nThreads);
#ifdef _DEBUG
		cout << "file thread ->" << i << "  -> [" << startOff << "  |  " << endOff << ")" << endl;
#endif
		packages.push_back(move(FileTaskPackage(directories.begin() + startOff, directories.cbegin() + endOff)));
	}


	vector<future<void>> results;

	for (auto& package : packages)
	{
		results.emplace_back(tp.push(&Grepper::scan_file_package_tp, this, ref(package)));
	}

	for (auto&& result : results)
		result.get();


	//concatenate reports
	for (auto const& package : packages)
	{
		_report.insert(_report.end(), package.reports.begin(), package.reports.end());
	}

	//get the total match num
	for (auto& item : _report)
	{
		for (auto const& l : item.lines)
		{
			_total_count += l.match_num;
		}
	}
	sort_report();
}

void Grepper::scan_file_package_tp(FileTaskPackage& filePackage)
{
	for (auto it = filePackage.begin; it != filePackage.end; it++)
	{
		if (is_directory((*it).status()))
		{
			if (_verbose)
				Grepper::locked_cout("\nScanning " + (*it).path().string() + "\n");
		}
		else
		{

			//if extension matches the Regex, generate a ReportItem
			if (regex_match((*it).path().extension().u8string().erase(0, 1), reg_extensions))
			{
				{
					if (_verbose)
						Grepper::locked_cout("\nGrepping " + (*it).path().string() + "\n");
				}

				string line;

				ReportItem newReport;
				newReport.file_name = (*it).path().string();

				vector<string> lines;

				{
					ifstream file((*it).path());

					while (getline(file, line))
					{
						lines.push_back(move(line));
					}
				}

				//get num of threads
				SYSTEM_INFO si;
				GetSystemInfo(&si);
				auto nThreads = si.dwNumberOfProcessors; //dw number is DWORD, DWORD is unsigned long

				//process the lines using MT
				size_t nElements = lines.size();

				//Create tasks
				vector<LineTaskPacage> linePackages;

				//generate line groups
				for (size_t i = 0; i < nThreads; i++)
				{
					size_t startOff = size_t(i * nElements / nThreads);
					size_t endOff = size_t((i + 1) * nElements / nThreads);
#ifdef _DEBUG
					cout << "line thread ->" << i << "  -> [" << startOff << "  |  " << endOff << ")" << endl;
#endif // DEBUG

					linePackages.push_back(move(LineTaskPacage(startOff, endOff, &lines)));
				}

				vector<future<void>> results;

				for (auto& linepackage : linePackages)
				{
					results.emplace_back(tp.push(&Grepper::scan_line_package, this, (*it).path().string(), ref(linepackage)));
				}

				for (auto&& result : results)
					result.get();

				for (auto& linepackage : linePackages)
				{
					if (!linepackage.reportItemLines.empty())
					{
						newReport.lines.insert(newReport.lines.end(), linepackage.reportItemLines.begin(), linepackage.reportItemLines.end());
					}
				}

				if (!newReport.lines.empty())
				{
					filePackage.reports.push_back(newReport);
				}
			}
		}
	}
}